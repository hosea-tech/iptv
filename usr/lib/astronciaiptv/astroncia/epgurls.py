# pylint: disable=missing-module-docstring
# SPDX-License-Identifier: GPL-3.0-only
EPG_URLS = [
    "http://www.teleguide.info/download/new3/xmltv.xml.gz",
    "https://iptvx.one/EPG_LITE",
    "http://programtv.ru/xmltv.xml.gz",
    "http://epg.it999.ru/edem.xml.gz",
    "https://iptv-org.github.io/epg/guides/ru/tv.yandex.ru.epg.xml",
    "https://iptv-org.github.io/epg/guides/ba/mtel.ba.epg.xml",
    "https://iptv-org.github.io/epg/guides/ba/tvarenasport.com.epg.xml",
    "https://iptv-org.github.io/epg/guides/hr/maxtv.hrvatskitelekom.hr.epg.xml",
    "https://iptv-org.github.io/epg/guides/hr/tvarenasport.hr.epg.xml",
    "https://iptv-org.github.io/epg/guides/xk/tvim.tv.epg.xml",
    "https://iptv-org.github.io/epg/guides/me/tvarenasport.com.epg.xml",
    "https://iptv-org.github.io/epg/guides/mk/maxtvgo.mk.epg.xml",
    "https://iptv-org.github.io/epg/guides/mk/tvarenasport.com.epg.xml",
    "https://iptv-org.github.io/epg/guides/rs/mts.rs.epg.xml",
    "https://iptv-org.github.io/epg/guides/rs/tvarenasport.com.epg.xml",
    "https://iptv-org.github.io/epg/guides/si/tv2go.t-2.net.epg.xml",
    "https://github.com/shaxxx/epg/raw/main/output/nuno.xml.gz",
    "http://epg.nextgen-iptv.com/epg.xml",
    "http://cdn.iptvhr.net/tvdata/guide.xml"    
]
