# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the astronciaiptv package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-21 01:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-21 01:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

msgid "name"
msgstr ""

msgid "m3u_ready"
msgstr ""

msgid "m3u_m3ueditor"
msgstr ""

msgid "m3u_noname"
msgstr ""

msgid "m3u_waschanged"
msgstr ""

msgid "m3u_saveconfirm"
msgstr ""

msgid "m3u_file"
msgstr ""

msgid "m3u_loadm3u"
msgstr ""

msgid "m3u_saveas"
msgstr ""

msgid "m3u_find"
msgstr ""

msgid "m3u_replacewith"
msgstr ""

msgid "m3u_replaceall"
msgstr ""

msgid "m3u_deleterow"
msgstr ""

msgid "m3u_addrow"
msgstr ""

msgid "m3u_movedown"
msgstr ""

msgid "m3u_moveup"
msgstr ""

msgid "m3u_filtergroup"
msgstr ""

msgid "m3u_searchterm"
msgstr ""

msgid "m3u_choosecolumn"
msgstr ""

msgid "m3u_openfile"
msgstr ""

msgid "m3u_playlists"
msgstr ""

msgid "m3u_loaded"
msgstr ""

msgid "m3u_savefile"
msgstr ""

msgid "m3u_m3ufiles"
msgstr ""

msgid "error"
msgstr ""

msgid "playerror"
msgstr ""

msgid "error2"
msgstr ""

msgid "starting"
msgstr ""

msgid "binarynotfound"
msgstr ""

msgid "nocacheplaylist"
msgstr ""

msgid "loadingplaylist"
msgstr ""

msgid "playlistloaderror"
msgstr ""

msgid "playlistloaddone"
msgstr ""

msgid "cachingplaylist"
msgstr ""

msgid "playlistcached"
msgstr ""

msgid "usingcachedplaylist"
msgstr ""

msgid "settings"
msgstr ""

msgid "help"
msgstr ""

msgid "channelsettings"
msgstr ""

msgid "selectplaylist"
msgstr ""

msgid "selectepg"
msgstr ""

msgid "selectwritefolder"
msgstr ""

msgid "deinterlace"
msgstr ""

msgid "useragent"
msgstr ""

msgid "empty"
msgstr ""

msgid "channel"
msgstr ""

msgid "savesettings"
msgstr ""

msgid "save"
msgstr ""

msgid "m3uplaylist"
msgstr ""

msgid "updateatboot"
msgstr ""

msgid "epgaddress"
msgstr ""

msgid "udpproxy"
msgstr ""

msgid "writefolder"
msgstr ""

msgid "orselectyourprovider"
msgstr ""

msgid "resetchannelsettings"
msgstr ""

msgid "notselected"
msgstr ""

msgid "close"
msgstr ""

msgid "nochannelselected"
msgstr ""

msgid "pause"
msgstr ""

msgid "play"
msgstr ""

msgid "exitfullscreen"
msgstr ""

msgid "volume"
msgstr ""

msgid "volumeoff"
msgstr ""

msgid "select"
msgstr ""

msgid "tvguide"
msgstr ""

msgid "startrecording"
msgstr ""

msgid "loading"
msgstr ""

msgid "doingscreenshot"
msgstr ""

msgid "screenshotsaved"
msgstr ""

msgid "screenshotsaveerror"
msgstr ""

msgid "notvguideforchannel"
msgstr ""

msgid "preparingrecord"
msgstr ""

msgid "nochannelselforrecord"
msgstr ""

msgid "stop"
msgstr ""

msgid "fullscreen"
msgstr ""

msgid "openrecordingsfolder"
msgstr ""

msgid "record"
msgstr ""

msgid "screenshot"
msgstr ""

msgid "prevchannel"
msgstr ""

msgid "nextchannel"
msgstr ""

msgid "tvguideupdating"
msgstr ""

msgid "tvguideupdatingerror"
msgstr ""

msgid "tvguideupdatingdone"
msgstr ""

msgid "recordwaiting"
msgstr ""

msgid "allchannels"
msgstr ""

msgid "favourite"
msgstr ""

msgid "interfacelang"
msgstr ""

msgid "tvguideoffset"
msgstr ""

msgid "hours"
msgstr ""

msgid "hwaccel"
msgstr ""

msgid "sort"
msgstr ""

msgid "sortitems1"
msgstr ""

msgid "sortitems2"
msgstr ""

msgid "sortitems3"
msgstr ""

msgid "sortitems4"
msgstr ""

msgid "donotforgetsort"
msgstr ""

msgid "moresettings"
msgstr ""

msgid "lesssettings"
msgstr ""

msgid "enabled"
msgstr ""

msgid "disabled"
msgstr ""

msgid "seconds"
msgstr ""

msgid "cache"
msgstr ""

msgid "chansearch"
msgstr ""

msgid "reconnecting"
msgstr ""

msgid "group"
msgstr ""

msgid "hide"
msgstr ""

msgid "contrast"
msgstr ""

msgid "brightness"
msgstr ""

msgid "hue"
msgstr ""

msgid "saturation"
msgstr ""

msgid "gamma"
msgstr ""

msgid "timeshift"
msgstr ""

msgid "tab_main"
msgstr ""

msgid "tab_video"
msgstr ""

msgid "tab_network"
msgstr ""

msgid "tab_other"
msgstr ""

msgid "mpv_options"
msgstr ""

msgid "donotupdateepg"
msgstr ""

msgid "search"
msgstr ""

msgid "tab_gui"
msgstr ""

msgid "epg_gui"
msgstr ""

msgid "classic"
msgstr ""

msgid "simple"
msgstr ""

msgid "simple_noicons"
msgstr ""

msgid "update"
msgstr ""

msgid "scheduler"
msgstr ""

msgid "choosechannel"
msgstr ""

msgid "bitrate1"
msgstr ""

msgid "bitrate2"
msgstr ""

msgid "bitrate3"
msgstr ""

msgid "bitrate4"
msgstr ""

msgid "bitrate5"
msgstr ""

msgid "starttime"
msgstr ""

msgid "endtime"
msgstr ""

msgid "addrecord"
msgstr ""

msgid "delrecord"
msgstr ""

msgid "plannedrec"
msgstr ""

msgid "warningstr"
msgstr ""

msgid "status"
msgstr ""

msgid "recnothing"
msgstr ""

msgid "recwaiting"
msgstr ""

msgid "recrecording"
msgstr ""

msgid "activerec"
msgstr ""

msgid "page"
msgstr ""

msgid "fasterview"
msgstr ""

msgid "playlists"
msgstr ""

msgid "provselect"
msgstr ""

msgid "provadd"
msgstr ""

msgid "provedit"
msgstr ""

msgid "provdelete"
msgstr ""

msgid "provname"
msgstr ""

msgid "of"
msgstr ""

msgid "importhypnotix"
msgstr ""

msgid "channelsonpage"
msgstr ""

msgid "resetdefplaylists"
msgstr ""

msgid "license"
msgstr ""

msgid "openprevchan"
msgstr ""

msgid "smscheduler"
msgstr ""

msgid "praction"
msgstr ""

msgid "nothingtodo"
msgstr ""

msgid "stoppress"
msgstr ""

msgid "turnoffpc"
msgstr ""

msgid "exitprogram"
msgstr ""

msgid "nohypnotixpf"
msgstr ""

msgid "theme"
msgstr ""

msgid "videoaspect"
msgstr ""

msgid "default"
msgstr ""

msgid "zoom"
msgstr ""

msgid "openexternal"
msgstr ""

msgid "open"
msgstr ""

msgid "remembervol"
msgstr ""

msgid "hidempv"
msgstr ""

msgid "filepath"
msgstr ""

msgid "panscan"
msgstr ""

msgid "tab_exp"
msgstr ""

msgid "exp1"
msgstr ""

msgid "exp2"
msgstr ""

msgid "exp3"
msgstr ""

msgid "expwarning"
msgstr ""

msgid "mouseswitchchannels"
msgstr ""

msgid "actions"
msgstr ""

msgid "defaultchangevol"
msgstr ""

msgid "showplaylistmouse"
msgstr ""

msgid "showcontrolsmouse"
msgstr ""

msgid "volumeshort"
msgstr ""

msgid "hidetvguide"
msgstr ""

msgid "flpopacity"
msgstr ""

msgid "showhideplaylist"
msgstr ""

msgid "showhidectrlpanel"
msgstr ""

msgid "mininterface"
msgstr ""

msgid "panelposition"
msgstr ""

msgid "left"
msgstr ""

msgid "right"
msgstr ""

msgid "doscreenshotsvia"
msgstr ""

msgid "playlistsep"
msgstr ""

msgid "volumechangestep"
msgstr ""

msgid "hideepgpercentage"
msgstr ""

msgid "xtreamnoconn"
msgstr ""

msgid "procerror"
msgstr ""

msgid "username"
msgstr ""

msgid "password"
msgstr ""

msgid "url"
msgstr ""

msgid "multipleplnote"
msgstr ""

msgid "favoritesplaylistsep"
msgstr ""

msgid "nochannels"
msgstr ""

msgid "epgname"
msgstr ""

msgid "epgid"
msgstr ""

msgid "iaepgmatch"
msgstr ""

msgid "using"
msgstr ""

msgid "Aspect"
msgstr ""

msgid "Average Bitrate"
msgstr ""

msgid "Channel Count"
msgstr ""

msgid "Channels"
msgstr ""

msgid "Codec"
msgstr ""

msgid "colour"
msgstr ""

msgid "Dimensions"
msgstr ""

msgid "Format"
msgstr ""

msgid "Gamma"
msgstr ""

msgid "general"
msgstr ""

msgid "layout"
msgstr ""

msgid "Pixel Format"
msgstr ""

msgid "Sample Rate"
msgstr ""

msgid "Stream Information"
msgstr ""

msgid "Video"
msgstr ""

msgid "Audio"
msgstr ""

msgid "checkforupdates"
msgstr ""

msgid "gotlatestversion"
msgstr ""

msgid "newversiongetfail"
msgstr ""

msgid "newversionavail"
msgstr ""

msgid "aboutqt"
msgstr ""

msgid "applog"
msgstr ""

msgid "mpvlog"
msgstr ""

msgid "copytoclipboard"
msgstr ""

msgid "choosesavefilename"
msgstr ""

msgid "logs"
msgstr ""

msgid "Bits Per Pixel"
msgstr ""

msgid "epgloading"
msgstr ""

msgid "unknownencoding"
msgstr ""

msgid "menubar_exit"
msgstr ""

msgid "menubar_title_file"
msgstr ""

msgid "menubar_title_play"
msgstr ""

msgid "menubar_playpause"
msgstr ""

msgid "menubar_stop"
msgstr ""

msgid "minutes_plural"
msgid_plural ""
msgstr[0] ""
msgstr[1] ""

msgid "seconds_plural"
msgid_plural ""
msgstr[0] ""
msgstr[1] ""

msgid "menubar_previous"
msgstr ""

msgid "menubar_next"
msgstr ""

msgid "speed"
msgstr ""

msgid "menubar_normalspeed"
msgstr ""

msgid "menubar_video"
msgstr ""

msgid "menubar_audio"
msgstr ""

msgid "menubar_fullscreen"
msgstr ""

msgid "menubar_compactmode"
msgstr ""

msgid "menubar_csforchannel"
msgstr ""

msgid "menubar_view"
msgstr ""

msgid "menubar_options"
msgstr ""

msgid "menubar_settings"
msgstr ""

msgid "menubar_about"
msgstr ""

msgid "menubar_help"
msgstr ""

msgid "menubar_screenshot"
msgstr ""

msgid "menubar_mute"
msgstr ""

msgid "menubar_track"
msgstr ""

msgid "empty_sm"
msgstr ""

msgid "menubar_checkupdates"
msgstr ""

msgid "ffmpeg_processing"
msgstr ""

msgid "menubar_volumeminus"
msgstr ""

msgid "menubar_volumeplus"
msgstr ""

msgid "list"
msgstr ""

msgid "menubar_m3ueditor"
msgstr ""

msgid "menubar_playlists"
msgstr ""

msgid "menubar_channelsort"
msgstr ""

msgid "menubar_filters"
msgstr ""

msgid "menubar_postproc"
msgstr ""

msgid "menubar_deblock"
msgstr ""

msgid "menubar_dering"
msgstr ""

msgid "menubar_debanding"
msgstr ""

msgid "menubar_noise"
msgstr ""

msgid "menubar_black"
msgstr ""

msgid "menubar_phase"
msgstr ""

msgid "menubar_extrastereo"
msgstr ""

msgid "menubar_karaoke"
msgstr ""

msgid "menubar_earvax"
msgstr ""

msgid "menubar_volnorm"
msgstr ""

msgid "errorvfapply"
msgstr ""

msgid "menubar_softscaling"
msgstr ""

msgid "menubar_updateepg"
msgstr ""

msgid "playername"
msgstr ""

msgid "hidebitrateinfo"
msgstr ""

msgid "movedragging"
msgstr ""

msgid "playlist"
msgstr ""

msgid "ignoringmpvoptions"
msgstr ""

msgid "showonlychplaylist"
msgstr ""

msgid "channels"
msgstr ""

msgid "hideplaylistleftclk"
msgstr ""

msgid "styleredefoff"
msgstr ""

msgid "alwaysontop"
msgstr ""

msgid "expfunctionwarning"
msgstr ""

msgid "menubar_expfunction"
msgstr ""

msgid "autoreconnection"
msgstr ""

msgid "customua"
msgstr ""

msgid "deletefromfav"
msgstr ""

msgid "scrrecnosubfolders"
msgstr ""

msgid "nocacheepg"
msgstr ""

msgid "nourlset"
msgstr ""

msgid "updcurplaylist"
msgstr ""

msgid "shortcuts"
msgstr ""

msgid "shortcut"
msgstr ""

msgid "desc"
msgstr ""

msgid "exitfullscreen2"
msgstr ""

msgid "showclock"
msgstr ""

msgid "tvguideforallchans"
msgstr ""

msgid "shortcutchange"
msgstr ""

msgid "modifyshortcut"
msgstr ""

msgid "pressnewkey"
msgstr ""

msgid "cancel"
msgstr ""

msgid "ok"
msgstr ""

msgid "shortcutused"
msgstr ""

msgid "resettodefaults"
msgstr ""

msgid "areyousure"
msgstr ""

msgid "usingmode"
msgstr ""

msgid "catchup"
msgstr ""

msgid "enablecatchup"
msgstr ""

msgid "writingepgcache"
msgstr ""

msgid "movies"
msgstr ""

msgid "series"
msgstr ""

msgid "back"
msgstr ""

msgid "tvchannels"
msgstr ""

msgid "nothingfound"
msgstr ""

msgid "searchmovie"
msgstr ""

msgid "searchserie"
msgstr ""

msgid "hidetvprogram"
msgstr ""

msgid "m3u_playlistsaved"
msgstr ""

msgid "channellogos"
msgstr ""

msgid "preferm3u"
msgstr ""

msgid "preferepg"
msgstr ""

msgid "donotloadfromepg"
msgstr ""

msgid "donotloadanylogos"
msgstr ""

msgid "subtitles"
msgstr ""

msgid "off"
msgstr ""

msgid "incompatiblewithspw"
msgstr ""

msgid "menubar_subtitles"
msgstr ""

msgid "videosettings"
msgstr ""

msgid "hidech"
msgstr ""

msgid "menubar_videosettings"
msgstr ""

msgid "helptext"
msgstr ""
